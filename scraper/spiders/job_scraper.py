import scrapy
import re

class QuotesSpider(scrapy.Spider):
    name = "fed_reserve_jobs"

    def start_requests(self):
        urls = [
            'https://www.federalreserve.gov/start-job-search.htm',
        ]
        
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def getIframe(self, response):
        html = response.body
        keyword = ""
        category = ""
        if getattr(self, 'keywords',''):
            keyword = getattr(self, 'keywords', '')
        if getattr(self, 'category',''):
            category = getattr(self, 'category', '')
        for match in re.finditer("Submission for the position:", html):
            end = int(match.end())
            buffer_text = html[end:end+100]
            if len(keyword) > 0:
                if buffer_text.count(keyword) > 0:
                    print re.search(r'(.*?),', buffer_text).group(1).replace("'", "")
            else:
                print re.search(r'(.*?),', buffer_text).group(1).replace("'", "")

    def parse(self, response):
        iframe_url = response.css("iframe::attr(src)").extract()
        for url in iframe_url:
            yield scrapy.Request(url=url, callback=self.getIframe)
        
